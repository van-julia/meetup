# VanJulia meetup

We use this repository as our main coordination point for the
[Julia](https://en.wikipedia.org/wiki/Julia_\(programming_language\)) Meetup in **Vancouver**.
We try to leverage Julia's carefully crafted runtime and linguistic abstractions to build better and faster numerical
code for state-of-the-art research, fun and profit.
This group is **open for everybody**, beginners included, and we don't discriminate agains physical or IP addresses.
Please subscribe to our [**mailing list**](https://lists.ubc.ca/scripts/wa.exe?A0=JULIA-MEETUP)
and the [**issues**](https://gitlab.com/van-julia/meetup/-/issues) for further details,
and open a new issue if you have any questions or suggestions.
There is also an accompanying [meetup.com group](https://www.meetup.com/Programming-in-Julia/) to
reach more users, but we will keep the main information here in order to be more
flexible and to stay independent of their proprietary platform.

We hold meetings via [Jitsi](https://meet.jit.si/) on a **monthly** basis.
Please send an email to beronov \a\ cs.ubc.ca if you are interested in participating.
There are **no recordings**, as we want to keep the character of an informal meetup instead of an official seminar,
but notes and references will be posted below.
Before the pandemic, we used to meet in
the [Institute of Applied Mathematics](https://www.iam.ubc.ca/) (IAM) Lounge on UBC campus, located in 
room 306 of the [Leonard S. Klink building](
  https://www.openstreetmap.org/directions?from=&to=49.26543%2C-123.25539#map=17/49.26541/-123.25726).


# Some resources

- Community: [Discourse](https://discourse.julialang.org/), [Gitter](https://gitter.im/JuliaLang/julia), [Zulip](https://julialang.zulipchat.com/), [YouTube](https://www.youtube.com/channel/UC9IuUwwE2xdjQUT_LMLONoA)
- Editors/IDEs/notebooks: [editor support](https://github.com/JuliaEditorSupport/),
[VSCode](https://www.julia-vscode.org/), [Pluto](https://juliahub.com/docs/Pluto/), [IJulia](https://julialang.github.io/IJulia.jl/stable/)
- Numerical libraries: [Optimization](https://www.juliaopt.org/), [Machine Learning](https://fluxml.ai/),
  [Probabilistic Programming](https://juliapackages.com/c/probabilistic-programming)
- Application libraries: [Economics](https://julia.quantecon.org/)


# Planned meetups

## [2021 TBD] Automatic differentiation

We will talk about the theory of AD, as well as the high-level designs of differentiable programming libraries,
e.g.,
- [Flux](https://fluxml.ai/)/[Zygote](https://github.com/FluxML/Zygote.jl)
- [ChainRules](https://www.juliadiff.org/ChainRulesCore.jl/stable/)
- Diffractor.jl ([talk](https://www.youtube.com/watch?v=mQnSRfseu0c),
[mathematical terminology](https://gist.github.com/Keno/4a6507b75288b1fe671e9d1cc683014f),
[opaque closures PR](https://github.com/JuliaLang/julia/pull/37849))


## [2021 TBD] Parallelism

We will talk about the important and complex topic of parallelism. Julia provides many kinds of parallelism and concurrency via different
native mechanisms, and there is a variety of packages trying to provide convenient interfaces to this power. We could maybe start
with a [basic tutorial](https://juliafolds.github.io/data-parallelism/tutorials/quick-introduction/) on data parallelism,
which is arguably the simplest way to think about parallelism, and defer more complex topics to future sessions.


## [2021 TBD] Plotting


# Past meetups

## [2021 April 19th, 3 pm PDT] Extensible symbolic programming in Julia

- **Speaker:** [Shashi Gowda](http://shashi.biz/), lead developer of [SymbolicUtils.jl](https://symbolicutils.juliasymbolics.org/)
- **Abstract:**
  This talk is about symbolic programming in Julia using the [Symbolics.jl](https://symbolics.juliasymbolics.org/dev/) package.
  To begin with, we will look at some of the features we have in the *Symbolics.jl* package. We will compare this to other existing symbolic programming packages both in Julia and in other languages.
  Then we will dive into how the system works under the hood: we will look at how to represent symbolic expressions, how it can be made efficient, rule-based rewriting, and the algebraic simplification problem.
  Finally, we will consider extensibility. [CAS](https://en.wikipedia.org/wiki/Computer_algebra_system) is a really big field of research, and no one package can bring all the features that a symbolic ecosystem will ever need. We discuss an interface that packages across the Julia ecosystem can use to utilize the expression rewriting machinery of *SymbolicUtils.jl* to rewrite their own representations, and further a possible way of even being able to swap out the rewriting machinery.
- [**Material**](https://gist.github.com/shashi/419058d8840ac789fe0829b84157949a)


## [2021 February 26th, 10 am PST] Introduction to Ordinary Differential Equations

- **Speaker:** Juliane Weilbach
- **Bio:** Juliane is studying Computer Science at the [University of Freiburg](https://uni-freiburg.de/en/). She is currently doing her Master's thesis at [Bosch Center for Artificial Intelligence](https://www.bosch-ai.com/) and is programming with Julia in the area of dynamical systems.
- **Abstract:**
  - How to implement ODEs with [DifferentialEquations.jl](https://github.com/SciML/DifferentialEquations.jl)
  - Brief introduction to Bayesian Optimization for parameter estimation of ODEs
  - How to use Python packages from Julia
- [**Material**](/material/2021.02-Intro_to_ODEs.ipynb)


## [2020 December 14th, 3 pm PST] A computational statistician's first computational project: [Parallel tempering](https://en.wikipedia.org/wiki/Parallel_tempering) in Julia

- **Speaker:** [Saifuddin Syed](https://www.stat.ubc.ca/~saif.syed/)
- **Abstract:**
 I will give a brief introduction to parallel tempering and discuss how I efficiently implemented it in Julia. In particular, I will use my project as a case study to emphasize how designing code in Julia differs from Python, give tips/resources I found useful to improve performance, discuss some useful conventions, and environments to start exploring Julia. This talk is directed at people that are relatively new to Julia.
- **Notes:**
  - Introduction round
  - Introduction to parallel tempering
    - The problem of multi-modality in real-world statistical inference
    - Parallelization strategy: [MCMC](https://en.wikipedia.org/wiki/Markov_chain_Monte_Carlo)
      at different target distributions, interpolating from easy (exploration) to hard (exploitation),
      communicating via swapping particles (or rather pointers)
    - Animated visualization of parallel tempering live in action
    - Paper: ["Non-Reversible Parallel Tempering: a Scalable Highly Parallel MCMC Scheme"](https://arxiv.org/abs/1905.02939)
  - Amazed by the Julia experience
    - Learned the basics in a week, overcame a decade of fear of programming
    - Low-level enough for understanding, high-level enough for productivity
    - Very first implementation: an order of magnitude faster than in Python
    - Ecosystem, library designs: very forgiving, straight-forward and transparent, pleasant to use
  - Recommended resources on Julia
    - MIT course ["Parallel Computing and Scientific Machine Learning"](https://github.com/mitmath/18337)
      ([videos](https://www.youtube.com/channel/UCDtsHjkOEMHYPGgpKX8VOPg/videos))
    - [Syntax cheatsheet](https://cheatsheets.quantecon.org/)
    - [Style guide](https://github.com/johnmyleswhite/Style.jl)
    - [REPL tips and tricks](https://www.youtube.com/watch?v=EkgCENBFrAY&amp;index=9&amp;ab_channel=BrainRPGProject)
  - Project management in Julia
    - Scientific project assistant: [DrWatson](https://juliadynamics.github.io/DrWatson.jl/dev/)
    - Package structure: [Wikibook](https://en.wikibooks.org/wiki/Introducing_Julia/Modules_and_packages#Structure_of_a_package),
      [Pkg](https://tlienart.github.io/pub/julia/dev-pkg.html), [example](https://github.com/JuliaLang/Example.jl)
    - Interpreter: [OhMyREPL.jl](https://kristofferc.github.io/OhMyREPL.jl/latest/), [Revise.jl](https://timholy.github.io/Revise.jl/stable/),
      configuration via `.julia/config/startup.jl`
  - Numerics
    - Multiple dispatch is the foundation of everything
    - Plotting
      - [Plots.jl](https://docs.juliaplots.org/latest/) ([talk](https://youtu.be/LGB8GvAL4HA)) is very intuitive to use,
        powerful features include [recipes](https://docs.juliaplots.org/latest/recipes/),
        [layouts](https://docs.juliaplots.org/latest/layouts/) and [animations](https://docs.juliaplots.org/latest/animations/)
      - Others: [Gadfly.jl](http://gadflyjl.org/stable/), [UnicodePlots](https://docs.juliaplots.org/latest/examples/unicodeplots/), ...
    - Profiling: [BenchmarkTools.jl](https://github.com/JuliaCI/BenchmarkTools.jl),
      [PkgBenchmark.jl](https://github.com/JuliaCI/PkgBenchmark.jl),
      [BenchmarkCI.jl](https://github.com/tkf/BenchmarkCI.jl)
    - Controlling [bounds checks](https://docs.julialang.org/en/v1/devdocs/boundscheck/)
    - Essential for leveraging efficiency of type system: [type stability](https://en.wikibooks.org/wiki/Introducing_Julia/Types#Type_stability)
      (the output type can be inferred automatically from the input type)
  - Discussion
    - Academic productivity
    - Industrial use and language legacy effects
    - Importance of mathematical syntax for thinking across traditional community boundaries


## [2020 November 16th, 3 pm PST] Introduction to the [probabilistic programming](https://en.wikipedia.org/wiki/Probabilistic_programming) system [Gen.jl](https://www.gen.dev/)

- **Speaker:** [Brian Irwin](http://orcid.org/0000-0002-6086-4359)
- **Notes:**
  - Introduction round
  - Brief discussion about package management in Julia
  (notable mentions: [Docker](https://github.com/docker-library/julia), [Julia environments](https://julialang.github.io/Pkg.jl/latest/environments/))
  - Brief motivation of probabilistic modelling and probabilistic programming
  - Walk-through of [A Bottom-Up Introduction to Gen](https://www.gen.dev/tutorials/bottom-up/A%20Bottom-Up%20Introduction%20to%20Gen)
  - Brief discussion
    - Relationship between Bayesian inference and optimization
    - Comparison of *Gen.jl* and [Turing.jl](https://turing.ml/) in terms of design goals 


## [2020 March 12th, 5 pm PDT] The Julia compiler

- **Speakers:** [Boyan Beronov](https://scholar.google.com/citations?user=mJH2wncAAAAJ&hl=en&oi=ao), [Christian Weilbach](https://scholar.google.com/citations?user=6foQfZwAAAAJ&hl=en&oi=ao)
- **Notes:**
  - Introduction round
  - Presentation and discussion about Julia's type system design and JIT-compiler architecture
  - Resources
    - [Engineering Julia for Speed (JuliaCon 2018)](https://www.youtube.com/watch?v=XWIZ_dCO6X8)
    - [Julia: dynamism and performance reconciled by design (OOPSLA)](https://dl.acm.org/doi/10.1145/3276490)


## [2020 February 13th, 1 pm PST] Kickoff with tooling

- **Abstract:**
Julia tooling, e.g., editor support, workflows and useful ways to develop Julia code.
- **Notes:**
  - Introduction round
  - [Trevor Campbell](https://trevorcampbell.me/)'s experience: C++ smart with [Eigen](http://eigen.tuxfamily.org/), [R](https://www.r-project.org/) is a mess (12 crashes of interpreter in his introductory data science class), maybe Python is ok, but slow.
  - [Numpy](https://en.wikipedia.org/wiki/NumPy) vs. Julia, elementwise operations are often important, *Numpy* very expressive
  - Julia Pros
    - Fast!
    - Type system
    - Powerful Lisp-style meta-programming
    - Code looks like math
  - Julia Cons
    - Not really general purpose
    - JIT compiler overhead, e.g. for games
  - [Structured parallelism in Julia](https://discourse.julialang.org/t/question-regarding-julias-planned-approach-to-composable-safe-and-easy-multithreading/17692/6)
  - Live coding with [Michael Friedlander](https://friedlander.io/) on
  reloading of code and slow feedback loop, [Revise.jl](https://timholy.github.io/Revise.jl/stable/) slows interpreter startup
  - Problem: type redefinitions trigger errors and require interpreter restart
  - Maybe one can auto-generate new types or use parametric types to abstract that
